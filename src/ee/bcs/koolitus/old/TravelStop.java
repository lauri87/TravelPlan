package ee.bcs.koolitus.old;

import ee.bcs.koolitus.travelplan.TravelPlan;

public class TravelStop extends TravelPlan {

	private int stopId;
	private Double stopBudget;
	private String stopDate;

	public int getStopId() {
		return stopId;
	}

	public TravelStop setStopId(int stopId) {
		this.stopId = stopId;
		return this;
	}

	public Double getStopBudget() {
		return stopBudget;
	}

	public TravelStop setStopBudget(Double stopBudget) {
		this.stopBudget = stopBudget;
		return this;
	}

	public String getStopDatem() {
		return stopDate;
	}

	public TravelStop setStopDate(String stopDate) {
		this.stopDate = stopDate;
		return this;
	}

	
	
}
