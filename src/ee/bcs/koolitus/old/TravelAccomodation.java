package ee.bcs.koolitus.old;

import ee.bcs.koolitus.travelplan.TravelPlan;

public class TravelAccomodation extends TravelPlan {

	private int accomodationId;
	private Double accomodationBudget;
	private String accomodationDateFrom;
	private String accomodationDateTo;

	public int getAccomodationId() {
		return accomodationId;
	}

	public TravelAccomodation setAccomodationId(int accomodationId) {
		this.accomodationId = accomodationId;
		return this;
	}

	public Double getAccomodationBudget() {
		return accomodationBudget;
	}

	public TravelAccomodation setAccomodationBudget(Double accomodationBudget) {
		this.accomodationBudget = accomodationBudget;
		return this;
	}

	public String getAccomodationDateFrom() {
		return accomodationDateFrom;
	}

	public TravelAccomodation setAccomodationDateFrom(String accomodationDateFrom) {
		this.accomodationDateFrom = accomodationDateFrom;
		return this;
	}

	public String getAccomodationDateTo() {
		return accomodationDateTo;
	}

	public TravelAccomodation setAccomodationDateTo(String accomodationDateTo) {
		this.accomodationDateTo = accomodationDateTo;
		return this;
	}

	@Override
	public String toString() {
		return "TravelAccomodation [accomodationId=" + accomodationId + ", accomodationBudget=" + accomodationBudget
				+ ", accomodationDateFrom=" + accomodationDateFrom + ", accomodationDateTo=" + accomodationDateTo + "]";
	}

	
}
