package ee.bcs.koolitus.old;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.old.TravelStop;
import ee.bcs.koolitus.travelplan.db.DatabaseConnection;

public class TravelStopResource {
	public List<TravelStop> getAllTravelStops() {
		List<TravelStop> stops = new ArrayList<>();
		String sqlQuery = "SELECT * FROM travelplan.travel_stop";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				stops.add(new TravelStop().setStopId(results.getInt("travel__stop_id"))
						.setStopDate(results.getString("travel_stop_date"))
						.setStopBudget(results.getDouble("travel_stop_budget")));
						
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}

		return stops;
	}

}
