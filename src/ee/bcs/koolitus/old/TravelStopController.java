package ee.bcs.koolitus.old;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



@Path("/travelstops")
public class TravelStopController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TravelStop> getAllTravelStops() {
		TravelStopResource travelS = new TravelStopResource();
		List<TravelStop> stops = travelS.getAllTravelStops();
		return stops;
	}
}
