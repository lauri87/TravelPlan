package ee.bcs.koolitus.old;

import java.time.LocalDate;

import ee.bcs.koolitus.travelplan.TravelPlan;

public class TravelTransport extends TravelPlan {
	
	private int transportId;
	private TravelPlan getTravelId;
	private int travelType;
	private Double transportBudget;
	private LocalDate transportDateFrom;
	private LocalDate transportDateTo;
	private String transportDateFromString;
	private String transportDateToString;
	private String transportComment;

	public int getTransportId() {
		return transportId;
	}
	public TravelTransport setTransportId(int transportId) {
		this.transportId = transportId;
		return this;
	}

	public int getTravelType() {
		return travelType;
	}
	public TravelTransport setTravelType(int travelType) {
		this.travelType = travelType;
		return this;
	}
	public Double getTransportBudget() {
		return transportBudget;
	}
	public TravelTransport setTransportBudget(Double transportBudget) {
		this.transportBudget = transportBudget;
		return this;
	}
	public LocalDate getTransportDateFrom() {
		return transportDateFrom;
	}
	public TravelTransport setTransportDateFrom(LocalDate transportDateFrom) {
		this.transportDateFrom = transportDateFrom;
		return this;
	}
	public LocalDate getTransportDateTo() {
		return transportDateTo;
	}
	public TravelTransport setTransportDateTo(LocalDate transportDateTo) {
		this.transportDateTo = transportDateTo;
		return this;
	}
	public String getTransportDateFromString() {
		return transportDateFromString;
	}
	public TravelTransport setTransportDateFromString(String transportDateFromString) {
		this.transportDateFromString = transportDateFromString;
		return this;
	}
	public String getTransportDateToString() {
		return transportDateToString;
	}
	public TravelTransport setTransportDateToString(String transportDateToString) {
		this.transportDateToString = transportDateToString;
		return this;
	}
	public String getTransportComment() {
		return transportComment;
	}
	public TravelTransport setTransportComment(String transportComment) {
		this.transportComment = transportComment;
		return this;
	}
}
