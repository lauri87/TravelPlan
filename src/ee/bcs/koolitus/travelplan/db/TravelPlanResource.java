package ee.bcs.koolitus.travelplan.db;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.travelplan.TravelPlan;

public class TravelPlanResource {

	// meetod kõikide travelPlanide väljakutsumiseks
	public List<TravelPlan> getAllTravelPlans() {
		List<TravelPlan> plans = new ArrayList<>();
		String sqlQuery = "SELECT * FROM travelplan";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				plans.add(new TravelPlan().setTravelId(results.getInt("travel_id"))
						.setTravelName(results.getString("travel_name"))
						.setTravelBudget(
								results.getDouble("travel_budget") != 0.0 ? results.getDouble("travel_budget") : null)
						.setTravelComment(
								results.getString("travel_comment") != null ? results.getString("travel_comment")
										: null)
						.setTravelDateFrom(results.getDate("travel_date_from") != null
								? results.getDate("travel_date_from").toLocalDate()
								: null)
						.setTravelDateTo(results.getDate("travel_date_to") != null
								? results.getDate("travel_date_to").toLocalDate()
								: null));
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}

		return plans;
	}

	// meetod ID järgi otsimiseks
	public TravelPlan getTravelPlanById(int id) {
		TravelPlan plan = null;
		String sqlQuery = "SELECT * FROM travelplan WHERE travel_id = '" + id + "'";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
			while (results.next()) {
				plan = new TravelPlan().setTravelId(results.getInt("travel_id"))
						.setTravelName(results.getString("travel_name"))
						.setTravelDateFrom(results.getDate("travel_date_from").toLocalDate())
						.setTravelDateTo(results.getDate("travel_date_to").toLocalDate())
						.setTravelBudget(results.getDouble("travel_budget"))
						.setTravelComment(results.getString("travel_comment"));
			}

		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return plan;
	}

	// meetod nime järgi otsimiseks
	public TravelPlan getTravelPlanByName(String name) {
		TravelPlan plan = null;
		String sqlQuery = "SELECT * FROM travelplan WHERE travel_name = '" + name + "'";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
			while (results.next()) {
				plan = new TravelPlan().setTravelId(results.getInt("travel_id"))
						.setTravelName(results.getString("travel_name"))
						.setTravelDateFrom(results.getDate("travel_date_from").toLocalDate())
						.setTravelDateTo(results.getDate("travel_date_to").toLocalDate())
						.setTravelBudget(results.getDouble("travel_budget"))
						.setTravelComment(results.getString("travel_comment"));
			}

		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return plan;
	}

	// meetod TravelPlan-i lisamiseks
	public TravelPlan addTravelPlan(TravelPlan travelplan) {

		String sqlQuery = "INSERT INTO travelplan (travel_name, travel_date_from, travel_date_to, travel_budget, travel_comment) VALUES ('"
				+ travelplan.getTravelName() + "', "
				+ (travelplan.getTravelDateFrom() != null ? "'" + Date.valueOf(travelplan.getTravelDateFrom()) + "'"
						: null)
				+ ", "
				+ (travelplan.getTravelDateTo() != null ? "'" + Date.valueOf(travelplan.getTravelDateTo()) + "'" : null)
				+ ", " + (travelplan.getTravelBudget() != null ? +travelplan.getTravelBudget() : 0.0) + ", '"
				+ travelplan.getTravelComment() + "')";

		System.out.println(sqlQuery);
		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				travelplan.setTravelId(resultSet.getInt(1));
			}
			resultSet.close();
			// statement.close();
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}

		return travelplan;
	}

	// meetod uuendamiseks
	public void updateTravelPlan(TravelPlan travelplan) {
		String sqlQuery = "UPDATE travelplan SET " + "travel_name = '" + travelplan.getTravelName()
				+ "', travel_date_from = '" + travelplan.getTravelDateFrom() + "', travel_date_to = '"
				+ travelplan.getTravelDateTo() + "', travel_budget = '" + travelplan.getTravelBudget()
				+ "', travel_comment = '"
				+ travelplan.getTravelComment() + "' WHERE travel_id = " + travelplan.getTravelId();
		System.out.println(sqlQuery);
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Some error on update");
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
	}

	// meetod kustutamiseks
	public void deleteTravelPlan(TravelPlan travelplan) {
		String sqlQuery = "DELETE FROM travelplan WHERE travel_id = " + travelplan.getTravelId();
		System.out.println(sqlQuery);
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Some error on delete");
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
	}

}
