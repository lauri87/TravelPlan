package ee.bcs.koolitus.travelplan.db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.travelplan.TravelDetails;
import ee.bcs.koolitus.travelplan.db.DatabaseConnection;

public class TravelDetailsResource {

	public List<TravelDetails> getAllTravelDetails() {
		List<TravelDetails> details = new ArrayList<>();
		String sqlQuery = "SELECT \r\n" + "travelplan.*, travel_details.*, details_type.*, details_sub_type.*\r\n"
				+ "FROM (((travelplan\r\n"
				+ "LEFT JOIN travel_details ON travelplan.travel_id = travel_details.travel_id)\r\n"
				+ "LEFT JOIN details_sub_type ON travel_details.details_sub_type_id = details_sub_type.details_sub_type_id)\r\n"
				+ "LEFT JOIN details_type ON details_sub_type.details_type_id = details_type.details_type_id)\r\n"
				+ "ORDER BY travelplan.travel_id;";

		System.out.println(sqlQuery);
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				TravelDetails tD = new TravelDetails().setTravelDetailsId(results.getInt("travel_details_id"))
						.setDetailsTypeId(results.getInt("details_type_id"))
						.setDetailsType(results.getString("details_type"))
						.setDetailsSubTypeId(results.getInt("details_sub_type_id"))
						.setDetailsSubType(results.getString("details_sub_type"))
						.setDetailsBudget(
								results.getDouble("details_budget") != 0.0 ? results.getDouble("details_budget") : null)
						.setDetailsComment(
								results.getString("details_comment") != null ? results.getString("details_comment")
										: null)
						.setDetailsDateFrom(results.getDate("details_date_from") != null
								? results.getDate("details_date_from").toLocalDate()
								: null)
						.setDetailsDateTo(results.getDate("details_date_to") != null
								? results.getDate("details_date_to").toLocalDate()
								: null);
				tD.setTravelId(results.getInt("travel_id"));

				details.add(tD);
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		System.out.println(details);
		return details;
	}

// travel Details päring travel_Id järgi
	public List<TravelDetails> getTravelDetailsById(int id) {
		List<TravelDetails> detailsById = new ArrayList<>();
		String sqlQuery = "SELECT \r\n" + "travel_details.travel_details_id,\r\n" + "travel_details.travel_id,\r\n"
				+ "travel_details.details_sub_type_id, \r\n" + "details_type.details_type,\r\n"
				+ "details_type.details_type_id,\r\n" + "details_sub_type.details_sub_type, \r\n"
				+ "travel_details.details_comment, \r\n" + "travel_details.details_date_from, \r\n"
				+ "travel_details.details_date_to, \r\n" + "travel_details.details_budget\r\n"
				+ "FROM ((travel_details\r\n"
				+ "LEFT JOIN details_sub_type ON travel_details.details_sub_type_id = details_sub_type.details_sub_type_id)\r\n"
				+ "LEFT JOIN details_type ON details_sub_type.details_type_id = details_type.details_type_id)\r\n"
				+ "WHERE travel_details.travel_id = " + id;
		System.out.println(sqlQuery);
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
			while (results.next()) {
				TravelDetails tDI = new TravelDetails()
						// detailsById.add(new TravelDetails()
						.setTravelDetailsId(results.getInt("travel_details_id"))
						.setDetailsTypeId(results.getInt("details_type_id"))
						.setDetailsType(results.getString("details_type"))
						.setDetailsSubTypeId(results.getInt("details_sub_type_id"))
						.setDetailsSubType(results.getString("details_sub_type"))
						.setDetailsBudget(
								results.getDouble("details_budget") != 0.0 ? results.getDouble("details_budget") : null)
						.setDetailsComment(
								results.getString("details_comment") != null ? results.getString("details_comment")
										: null)
						.setDetailsDateFrom(results.getDate("details_date_from") != null
								? results.getDate("details_date_from").toLocalDate()
								: null)
						.setDetailsDateTo(results.getDate("details_date_to") != null
								? results.getDate("details_date_to").toLocalDate()
								: null);

				tDI.setTravelId(results.getInt("travel_id"));

				detailsById.add(tDI);
			}

		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return detailsById;
	}

	// meetod TravelDetails-i lisamiseks prepared statementiga
	public TravelDetails addTravelDetails(TravelDetails traveldetails) {

		String sqlQuery = "INSERT INTO travel_details \r\n"
				+ "(`travel_id`, `details_sub_type_id`, `details_date_from`, `details_date_to`, `details_budget`, `details_comment`) VALUES(?,?,?,?,?,?);";
		System.out.println(sqlQuery);
		try (Connection connection = DatabaseConnection.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, traveldetails.getTravelId());
			statement.setInt(2, traveldetails.getDetailsSubTypeId());
			statement.setDate(3,
					traveldetails.getDetailsDateFrom() != null ? Date.valueOf(traveldetails.getDetailsDateFrom())
							: null);
			statement.setDate(4,
					traveldetails.getDetailsDateTo() != null ? Date.valueOf(traveldetails.getDetailsDateTo()) : null);
			statement.setDouble(5, traveldetails.getDetailsBudget() != null ? traveldetails.getDetailsBudget() : 0.0);
			statement.setString(6, traveldetails.getDetailsComment());
			System.out.println("Prepared statement: " + statement);
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			resultSet.next();
			traveldetails.setTravelDetailsId(resultSet.getInt(1));
			resultSet.close();
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return traveldetails;
	}

	// meetod uuendamiseks
		public void updateTravelDetails(TravelDetails traveldetails) {
			String sqlQuery = 
					"UPDATE travelplan.travel_details SET " 
							+ "details_sub_type_id=" 
							+ traveldetails.getDetailsSubTypeId() + ", "
							+ "details_date_from=" 
							+ (traveldetails.getDetailsDateFrom() != null ? "'" + Date.valueOf(traveldetails.getDetailsDateFrom()) + "'" : null) + ", "
							+ "details_date_to =" 
							+ (traveldetails.getDetailsDateTo() != null ? "'" + Date.valueOf(traveldetails.getDetailsDateTo()) + "'" : null)  + ", "
							+ "details_budget=" 
							+ (traveldetails.getDetailsBudget() != null ? +traveldetails.getDetailsBudget() : 0.0) + ", "
							+ "details_comment='" 
							+ traveldetails.getDetailsComment()  
							+ "' WHERE travel_details_id = " + traveldetails.getTravelDetailsId();
			
			System.out.println(sqlQuery);
			try {
				Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
				if (code != 1) {
					throw new SQLException("Some error on update");
				}
			} catch (SQLException e) {
				System.out.println("Error on running query: " + e.getStackTrace());
			}
		}	


	
	// päring details_sub_type tabelisse type_id järgi
	public List<TravelDetails> getDetailsSubTypeById(int id) {
		List<TravelDetails> subTypes = new ArrayList<>();
		String sqlQuery = "SELECT * FROM travelplan.details_sub_type WHERE details_type_id =" + id;
		System.out.println(sqlQuery);
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				subTypes.add(new TravelDetails().setDetailsSubTypeId(results.getInt("details_sub_type_id"))
						.setDetailsTypeId(results.getInt("details_type_id"))
						.setDetailsSubType(results.getString("details_sub_type")));

			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return subTypes;

	}

	// meetod kustutamiseks
	public void deleteTravelDetails(TravelDetails traveldetails) {
		String sqlQuery = "DELETE FROM travelplan.travel_details WHERE travel_details_id = "
				+ traveldetails.getTravelDetailsId();
		System.out.println(sqlQuery);
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Some error on delete");
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
	}

}
