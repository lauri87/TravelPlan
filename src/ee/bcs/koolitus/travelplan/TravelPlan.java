package ee.bcs.koolitus.travelplan;

import java.time.LocalDate;

public class TravelPlan {
	private int travelId;
	private String travelName;
	private LocalDate travelDateFrom;
	private LocalDate travelDateTo;
	private String travelDateFromString;
	private String travelDateToString;
	private Double travelBudget;
	private String travelComment;

	public int getTravelId() {
		return travelId;
	}
	public TravelPlan setTravelId(int travelId) {
		this.travelId = travelId;
		return this;
	}
	public String getTravelName() {
		return travelName;
	}
	public TravelPlan setTravelName(String travelName) {
		this.travelName = travelName;
		return this;
	}
	public LocalDate getTravelDateFrom() {
		return travelDateFrom;
	}
	public TravelPlan setTravelDateFrom(LocalDate travelDateFrom) {
		this.travelDateFrom = travelDateFrom;
		return this;
	}
	public LocalDate getTravelDateTo() {
		return travelDateTo;
	}
	public TravelPlan setTravelDateTo(LocalDate travelDateTo) {
		this.travelDateTo = travelDateTo;
		return this;
	}
	public String getTravelDateFromString() {
		return travelDateFromString;
	}
	public TravelPlan setTravelDateFromString(String travelDateFromString) {
		this.travelDateFromString = travelDateFromString;
		if (travelDateFromString != null) {
			setTravelDateFromFromTravelDateFromString(travelDateFromString);
		}
		return this;
	}
	private void setTravelDateFromFromTravelDateFromString(String travelDateFromString) {
		this.travelDateFrom = LocalDate.parse(travelDateFromString);		
	}
	
	public String getTravelDateToString() {
		return travelDateToString;
	}
	public TravelPlan setTravelDateToString(String travelDateToString) {
		this.travelDateToString = travelDateToString;
		if (travelDateToString != null) {
			setTravelDateToFromTravelDateToString(travelDateToString);
		}
		return this;
	}
	private void setTravelDateToFromTravelDateToString(String travelDateToString) {
		this.travelDateTo = LocalDate.parse(travelDateToString);		
	}

	public Double getTravelBudget() {
		return travelBudget;
	}
	public TravelPlan setTravelBudget(Double travelBudget) {
		this.travelBudget = travelBudget;
		return this;
	}

	public String getTravelComment() {
		return travelComment;
	}
	public TravelPlan setTravelComment(String travelComment) {
		this.travelComment = travelComment;
		return this;
	}
	
	@Override
	public String toString() {
		return "TravelPlan [travelId=" + travelId + ", travelName=" + travelName + ", travelDateFrom="
				+ travelDateFrom + ", travelDateTo=" + travelDateTo + ", travelBudget=" + travelBudget + ", travelComment=" + travelComment + "]";
	}



}
