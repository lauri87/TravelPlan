package ee.bcs.koolitus.travelplan;

import java.util.List;

import ee.bcs.koolitus.travelplan.db.TravelDetailsResource;
import ee.bcs.koolitus.travelplan.db.TravelPlanResource;

public class Main {

	public static void main(String[] args) {
		TravelPlanResource allTravelPlans = new TravelPlanResource();
		List<TravelPlan> plans = allTravelPlans.getAllTravelPlans();
		System.out.println(plans); // prindib kogu tabeli

//		TravelPlan newTravelPlan = new TravelPlan()
//		.setTravelName("Oslo")
//		.setTravelDateFrom("2018-07-01")
//		.setTravelDateTo("2018-07-14")
//		.setTravelBudget(1000.00)
//		.setTravelFood(300.00);
//		System.out.println(allTravelPlans.addTravelType(newTravelPlan)); // prindib ainult loodud uue

		System.out.println("------------------");
		
		
		TravelDetailsResource allTravelDetails = new TravelDetailsResource();
		List<TravelDetails> details = allTravelDetails.getAllTravelDetails();
		System.out.println(details); //
		
	
	}

	
}
 