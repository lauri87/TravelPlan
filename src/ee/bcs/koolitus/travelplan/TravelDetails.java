package ee.bcs.koolitus.travelplan;

import java.time.LocalDate;

public class TravelDetails extends TravelPlan{

	private int travelDetailsId;
	private int detailsTypeId;
	private String detailsType;
	private int detailsSubTypeId;
	private String detailsSubType;
	private LocalDate detailsDateFrom;
	private LocalDate detailsDateTo;
	private String detailsDateFromString;
	private String detailsDateToString;
	private Double detailsBudget;
	private String detailsComment;

	public int getTravelDetailsId() {
		return travelDetailsId;
	}
	public TravelDetails setTravelDetailsId(int travelDetailsId) {
		this.travelDetailsId = travelDetailsId;
		return this;
	}
	public int getDetailsTypeId() {
		return detailsTypeId;
	}
	public TravelDetails setDetailsTypeId(int detailsTypeId) {
		this.detailsTypeId = detailsTypeId;
		return this;
	}
	public String getDetailsType() {
		return detailsType;
	}
	public TravelDetails setDetailsType(String detailsType) {
		this.detailsType = detailsType;
		return this;
	}
	public int getDetailsSubTypeId() {
		return detailsSubTypeId;
	}
	public TravelDetails setDetailsSubTypeId(int detailsSubTypeId) {
		this.detailsSubTypeId = detailsSubTypeId;
		return this;
	}
	public String getDetailsSubType() {
		return detailsSubType;
	}
	public TravelDetails setDetailsSubType(String detailsSubType) {
		this.detailsSubType = detailsSubType;
		return this;
	}
	public LocalDate getDetailsDateFrom() {
		return detailsDateFrom;
	}
	public TravelDetails setDetailsDateFrom(LocalDate detailsDateFrom) {
		this.detailsDateFrom = detailsDateFrom;
		return this;
	}
	public LocalDate getDetailsDateTo() {
		return detailsDateTo;
	}
	public TravelDetails setDetailsDateTo(LocalDate detailsDateTo) {
		this.detailsDateTo = detailsDateTo;
		return this;
	}
	public String getDetailsDateFromString() {
		return detailsDateFromString;
	}

	public TravelDetails setDetailsDateFromString(String detailsDateFromString) {
		this.detailsDateFromString = detailsDateFromString;
		if (detailsDateFromString != null) {
			setDetailsDateFromFromDetailsDateFromString(detailsDateFromString);
		}
		return this;
	}
	
	private void setDetailsDateFromFromDetailsDateFromString(String detailsDateFromString) {
		this.detailsDateFrom = LocalDate.parse(detailsDateFromString);		
	}
	
	public String getDetailsDateToString() {
		return detailsDateToString;
	}
	public TravelDetails setDetailsDateToString(String detailsDateToString) {
		this.detailsDateToString = detailsDateToString;
		if (detailsDateToString != null) {
			setDetailsDateToFromDetailsDateToString(detailsDateToString);
		}
		return this;
	}
	
	private void setDetailsDateToFromDetailsDateToString(String detailsDateToString) {
		this.detailsDateTo = LocalDate.parse(detailsDateToString);		
	}
	
	public Double getDetailsBudget() {
		return detailsBudget;
	}
	public TravelDetails setDetailsBudget(Double detailsBudget) {
		this.detailsBudget = detailsBudget;
		return this;
	}
	public String getDetailsComment() {
		return detailsComment;
	}
	public TravelDetails setDetailsComment(String detailsComment) {
		this.detailsComment = detailsComment;
		return this;
	}
	
	@Override
	public String toString() {
		return "TravelDetails [travelDetailsId=" + travelDetailsId + ", detailsTypeId=" + detailsTypeId
				+ ", detailsType=" + detailsType + ", detailsSubTypeId=" + detailsSubTypeId + ", detailsSubType="
				+ detailsSubType + ", detailsDateFrom=" + detailsDateFrom + ", detailsDateTo=" + detailsDateTo
				+ ", detailsDateFromString=" + detailsDateFromString + ", detailsDateToString=" + detailsDateToString
				+ ", detailsBudget=" + detailsBudget + ", detailsComment=" + detailsComment + "]";
	}

	
	
	
	
	
}
