package ee.bcs.koolitus.travelplan.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.bcs.koolitus.travelplan.TravelDetails;
import ee.bcs.koolitus.travelplan.db.TravelDetailsResource;

@Path("/traveldetails")
public class TravelDetailsController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TravelDetails> getAllTravelDetails() {
		TravelDetailsResource detailsR = new TravelDetailsResource();
		List<TravelDetails> details = detailsR.getAllTravelDetails();
		return details;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TravelDetails> getTravelDetailsById(@PathParam("id") int id) {
		TravelDetailsResource detailsById = new TravelDetailsResource();
		List<TravelDetails> details = detailsById.getTravelDetailsById(id);
		return details;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TravelDetails addTravelDetails(TravelDetails newDetails) {
		return new TravelDetailsResource().addTravelDetails(newDetails);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public TravelDetails updateTravelDetails(TravelDetails updateDetails) {
		new TravelDetailsResource().updateTravelDetails(updateDetails);
		return updateDetails;
	}

	@DELETE
	@Path("/{id}")
	public void deleteTravelDetails(@PathParam("id") int id) {
		new TravelDetailsResource().deleteTravelDetails(new TravelDetails().setTravelDetailsId(id));
	}


}
