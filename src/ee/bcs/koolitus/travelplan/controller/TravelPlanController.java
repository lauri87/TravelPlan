package ee.bcs.koolitus.travelplan.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.travelplan.TravelPlan;
import ee.bcs.koolitus.travelplan.db.TravelPlanResource;

@Path("/travelplans")
public class TravelPlanController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TravelPlan> getAllTravelPlans() {
		TravelPlanResource travelR = new TravelPlanResource();
		List<TravelPlan> plans = travelR.getAllTravelPlans();
		return plans;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public TravelPlan getTravelPlanById(@PathParam("id") int id) {
		return new TravelPlanResource().getTravelPlanById(id);
	}

	@GET
	@Path("/name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public TravelPlan getTravelPlanByName(@PathParam("name") String name) {
		return new TravelPlanResource().getTravelPlanByName(name);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TravelPlan addTravelPlan(TravelPlan newPlan) {
		return new TravelPlanResource().addTravelPlan(newPlan);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public TravelPlan updateTravelPlan(TravelPlan updatedPlan) {
		new TravelPlanResource().updateTravelPlan(updatedPlan);
		return updatedPlan;
	}

	@DELETE
	@Path("/{id}")
	public void deleteTravelPlan(@PathParam("id") int id) {
		new TravelPlanResource().deleteTravelPlan(new TravelPlan().setTravelId(id));
	}

}
