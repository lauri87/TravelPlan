var allTravelPlan = [];
function getAllPlans() {
	$
			.getJSON(
					"http://localhost:8080/TravelPlan/rest/travelplans",
					function(plans) {
						allTravelPlan = plans;
						var tableContent = "";
						for (var i = 0; i < plans.length; i++) {
							var containerName = "detailsTable" + i
							tableContent = tableContent
									+ "<tr>" 
//									+"<td>" + plans[i].travelId + "</td>" 
									+"<td>"
									+ plans[i].travelName
									+ "</td><td>"
									+ (plans[i].travelDateFrom != undefined ? plans[i].travelDateFrom
											: '')
									+ "</td><td>"
									+ (plans[i].travelDateTo != undefined ? plans[i].travelDateTo
											: '')
									+ "</td><td id='" + containerName + "BodytravelBudget'>"
									+ (plans[i].travelBudget != undefined ? plans[i].travelBudget
											: '')
									+ "</td><td class='longTextCell'>"
									+ (plans[i].travelComment != undefined ? plans[i].travelComment = travelCommentToUrl(plans[i].travelComment)
											: '' )
									+ "</td><td><button type='button' class='btn btn-default' onClick='viewDetailsTable("
									+ containerName
									+ ","
									+ plans[i].travelId
									+ ",\""
									+ plans[i].travelName
									+ "\")'>Details</button><button type='button' class='btn btn-default' data-toggle='modal' data-target='#updatePlanModal' onClick='fillChangePlanModal("
									+ plans[i].travelId
									+",\""
									+ plans[i].travelName
									+ "\")'>Change</button>"
									+ "<button type='button' class='btn btn-default' data-toggle='modal' data-target='#deletePlanModal' onClick='fillDeletePlanModal("
									+ plans[i].travelId
									+ ",\""
									+ plans[i].travelName
									+ "\")'>Delete</button></td></tr>"
									+ "<tr><td id='" + containerName
									+ "' colspan='8'></td></tr>";
						}
						document.getElementById("plansBody").innerHTML = tableContent;
					});
}
getAllPlans();



function addNewTravelPlan() {
	var newPlanJSON = $("#newTravelPlan").serializeFormJSON();
	var newPlan = JSON.stringify(newPlanJSON);
	$.ajax({
		url : "http://localhost:8080/TravelPlan/rest/travelplans",
		cache : false,
		type : 'POST',
		data : newPlan,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllPlans();
			$('#newPlanModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

//$.clearInput = function () {
//    $('newTravelPlan').find('input[type=text], input[type=date], input[type=number], textarea').val('');
//};
//
//$('#newPlanModal').on('hidden', function () {
//    $.clearInput();
//});

var planToChangeId;

function fillChangePlanModal(travelId, travelName) {
	for (var i = 0; i < allTravelPlan.length; i++) {
		if (allTravelPlan[i].travelId == travelId) {
//			document.getElementById("updateTravelId").value = travelId;
			document.getElementById("updateTravelName").value = allTravelPlan[i].travelName;
			document.getElementById("updateTravelDateFrom").value = allTravelPlan[i].travelDateFrom;
			document.getElementById("updateTravelDateTo").value = allTravelPlan[i].travelDateTo;
			document.getElementById("updateTravelBudget").value = allTravelPlan[i].travelBudget;
			document.getElementById("updateTravelComment").value = allTravelPlan[i].travelComment;
			planToChangeId = travelId;
		}
	}
	document.getElementById("changePlanModalLabel").innerHTML = "Change travelplan: " + travelName;
}

function savePlanChanges() {
	var updatePlanJSON = $("#updatePlanForm").serializeFormJSON();
	updatePlanJSON = addIdFieldInJson(updatePlanJSON, planToChangeId);
	var updatePlan = JSON.stringify(updatePlanJSON);

	$.ajax({
		url : "http://localhost:8080/TravelPlan/rest/travelplans",
		cache : false,
		type : 'PUT',
		data : updatePlan,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllPlans();
			$('#updatePlanModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

function addIdFieldInJson(objectJson, fieldValue) {
	var modifiedJson = objectJson;
	modifiedJson.travelId = fieldValue;
	return modifiedJson;
}

var deleteTravelId;
function fillDeletePlanModal(travelId, travelName) {
	document.getElementById("deleteModalBody").innerHTML = "Are you sure, you want to delete travelplan named "
			+ travelName + "?";
	deleteTravelId = travelId;
	document.getElementById("detelePlanModalLabel").innerHTML = "Delete " + travelName + " travel plan";
}

function deletePlan() {
	$.ajax({
		url : "http://localhost:8080/TravelPlan/rest/travelplans/"
				+ deleteTravelId,
		cache : false,
		type : 'DELETE',
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllPlans();
			$('#deletePlanModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

function requiredAdd(modal, checkTravelName) {  
    var travelName = document.forms[modal][checkTravelName].value;
    if (travelName == "") {  
        alert("Please insert travel name");  
        return false;  

    }  else if (modal == "newTravelPlan"){
    	addNewTravelPlan();
    } else {
    	savePlanChanges()
    	}
    }

function travelCommentToUrl (comment) {
	 var urlRegex = /(https?:\/\/[^\s]+)/g;
	    return comment.replace(urlRegex, function(url) {
	        return '<a href="' + url + '">' + url + '</a>';
     	    })
}

(function($) {
	$.fn.serializeFormJSON = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);
