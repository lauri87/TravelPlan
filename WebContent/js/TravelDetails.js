var allTravelDetails = [];
var selectedTravelId = [];
var selectedTravelName = [];
var travelTypes = [];
var travelTypeSubTypes = [];
var selectedDetailsSubTypeId;
var selectedTravelDetailsId;
var selectedContainerId;
var selectedDetailsBodyId;

function viewDetailsTable(containerName, travelId, travelName) {
	selectedTravelId = travelId;
	selectedTravelName = travelName;
	// containerName.innerHTML = buildDetailsTableBody(containerName);
	getAllDetails(containerName, travelId);
	selectedContainerId = containerName;

}
function getAllDetails(containerName, travelId) {
	$.getJSON(
			"http://localhost:8080/TravelPlan/rest/traveldetails/" + travelId,
			function(details) {
				allTravelDetails = details;
				containerName.innerHTML = buildDetailsTableBody(containerName,
						travelId);
				if (allTravelDetails.length > 0) {
					fillDetailsTable(containerName.id + "Body", travelId);
				}
			});
	getAllSubTypes();
}

function buildDetailsTableBody(containerName, travelId) {
	var detailsContent = "";

	if (allTravelDetails.length > 0) {
		detailsContent = detailsContent
				+ "<h3>Details</h3>"
				+ "<button type='button' class='close' onClick=closeDetails('" 
				+ containerName.id
				+ "') aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
				+ "<table class='table table-striped table bordered table-hover table table-condensed'>"
				+ "<thead>" + "<tr>"
				// + "<th>Id</th>"
				+ "<th onclick='sortByColumn(0,\"" + containerName.id
				+ "Body\")'>Date from</th>" + "<th onclick='sortByColumn(1,\""
				+ containerName.id + "Body\")'>Date to</th>"
				+ "<th onclick='sortByColumn(2,\"" + containerName.id
				+ "Body\")'>Type</th>" + "<th onclick='sortByColumn(3,\""
				+ containerName.id + "Body\")'>Sub type</th>"
				+ "<th onclick='sortByColumn(4,\"" + containerName.id
				+ "Body\")'>Budget</th>" + "<th>Comment</th>" + "<th></th>"
				+ "</tr>" + "</thead>" + "<tbody id='" + containerName.id
				+ "Body'>" + "</tbody>" + "<tfoot>" + "<td colspan='4'></td>"
				+ "<td colspan='3' id='" + containerName.id
				+ "BodybudgetTotal'></td>" + "</tfoot>" + "</table>";

	}
	detailsContent = detailsContent // close nupp lisada ka tühjale details tabelile, hetkel nähtav ainult juba isestatud andmete korral
			+ "<button type='button' class='btn btn-default' data-toggle='modal' data-target='#newDetailsModal' onClick='fillNewDetailsModal("
			+ selectedTravelId + ", \"" + selectedTravelName
			+ "\")'>Add details</button>";

	return detailsContent;
}

function fillDetailsTable(detailsBody, travelId) {
	details = allTravelDetails;
	if (details.length != 0) {
		var tableContent = "";
		for (var i = 0; i < details.length; i++) {
			tableContent = tableContent
					+ "<tr>"
					// + "<td>"
					// + details[i].travelDetailsId
					// + "</td>"
					+ "<td class='"
					+ detailsBody
					+ "detailsDateFrom'>"
					+ (details[i].detailsDateFrom != undefined ? details[i].detailsDateFrom
							: '')
					+ "</td><td class='"
					+ detailsBody
					+ "detailsDateTo'>"
					+ (details[i].detailsDateTo != undefined ? details[i].detailsDateTo
							: '')
					+ "</td><td class='"
					+ detailsBody
					+ "detailsType'>"
					+ (details[i].detailsType != undefined ? details[i].detailsType
							: '')
					+ "</td><td class='"
					+ detailsBody
					+ "detailsSubType'>"
					+ (details[i].detailsSubType != undefined ? details[i].detailsSubType
							: '')
					+ "</td><td class='"
					+ detailsBody
					+ "detailsBudget'>"
					+ (details[i].detailsBudget != undefined ? details[i].detailsBudget
							: '')
					+ "</td><td class ='breakComment'>"
					+ (details[i].detailsComment != undefined ? details[i].detailsComment = travelCommentToUrl(details[i].detailsComment)
							: '')
					+ "</td><td><button type='button' class='btn btn-default' data-toggle='modal' data-target='#updateDetailsModal' onClick='fillUpdateDetailsModal("
					+ details[i].travelDetailsId
					+ ","
					+ travelId
					+ ")'>Change</button><button type='button' class='btn btn-default' data-toggle='modal' data-target='#deleteDetailsModal' onClick='fillDeleteDetailsModal("
					+ details[i].travelDetailsId
					+ ")'>Delete</button></td></tr>";

		}

		document.getElementById(detailsBody).innerHTML = tableContent;
		detailsBudgetSum(detailsBody);
	}

	if (details.length != 0) {
		selectedDetailsBodyId = detailsBody;
	}
}

function sortByColumn(columnIndex, containerName) {
	var table = document.getElementById(containerName);
	var rows;
	var switching;
	var i;
	var x;
	var y;
	var shouldSwitch;
	var dir;
	var switchcount = 0;
	switching = true;
	dir = "asc";
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("tr");
		for (i = 0; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName("td")[columnIndex];
			y = rows[i + 1].getElementsByTagName("td")[columnIndex];
			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			switchcount++;
		} else {
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}

function closeDetails(containerName) {
	document.getElementById(containerName).innerHTML = '';
}

function detailsBudgetSum(detailsBody) {

	var tds = document.getElementById(detailsBody).getElementsByTagName('td');
	var sum = 0;
	for (var i = 4; i < tds.length; i = i + 7) {
		if (tds[i].className == detailsBody + 'detailsBudget') {
			sum += isNaN(tds[i].innerHTML) || tds[i].innerHTML == "" ? 0
					: parseInt(tds[i].innerHTML);
		}
	}
	var travelBudget = document.getElementById(detailsBody + 'travelBudget').innerText;
	var budgetLeft = parseInt(travelBudget - sum);
	document.getElementById(detailsBody + 'budgetTotal').innerHTML = '<b>Budget spent:</b> '
			+ sum + '<br/> <b>Budget left:</b> ' + budgetLeft;
}

function fillNewDetailsModal(travelId, travelName) {
	selectedTravelId = travelId;
	document.getElementById("newDetailsModalLabel").innerHTML = "Add details to "
			+ travelName + " travel plan";
}

function getNewDetailsSubTypeId() {
	subTypeId = document.getElementById("newSubTypeSelect").value;
	selectedDetailsSubTypeId = subTypeId;
}

function addIdFieldInJson(objectJson, selectedTravelId,
		selectedDetailsSubTypeId) {
	var modifiedJson = objectJson;
	modifiedJson.travelId = selectedTravelId;
	modifiedJson.detailsSubTypeId = selectedDetailsSubTypeId;
	return modifiedJson;
}
function addNewDetails() {
	getNewDetailsSubTypeId();
	var newDetailsJSON = $("#newDetails").serializeFormJSON();
	newDetailsJSON = addIdFieldInJson(newDetailsJSON, selectedTravelId,
			selectedDetailsSubTypeId);
	var newDetails = JSON.stringify(newDetailsJSON);
	$.ajax({
		url : "http://localhost:8080/TravelPlan/rest/traveldetails",
		cache : false,
		type : 'POST',
		data : newDetails,
		contentType : "application/json; charset=utf-8",
		success : function() {
			viewDetailsTable(selectedContainerId, selectedTravelId,
					selectedTravelName);
			$('#newDetailsModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

var allSubTypes = [];
function getAllSubTypes() {
	$.getJSON("http://localhost:8080/TravelPlan/rest/subtypes", function(
			subTypes) {
		allSubTypes = subTypes; // massiiv, milles on JSONist saadud andmed
	});
}

function getSubTypeByDetailsTypeId(detailsTypeId) {
	var subTypes = [];
	for (var i = 0; i < allSubTypes.length; i++) {
		if (allSubTypes[i].detailsTypeId == detailsTypeId) {
			subTypes.push(allSubTypes[i]);
		}
	}
	return subTypes;
}

function showTypes(selectId, subTypeSelect, detailsTypeId) {
	if (detailsTypeId == "") {
		document.getElementById("subType").innerHTML = "";
		return;
	}
	var subTypesList = getSubTypeByDetailsTypeId(detailsTypeId); // lisasime
	// getSubTypeId
	buildTypeSelect(selectId, subTypeSelect, subTypesList);
}

function buildTypeSelect(selectId, subTypeSelect, subTypes) {
	var typesContent = "";
	for (i = 0; i < subTypes.length; i++) {
		typesContent = typesContent + "<option value='"
				+ subTypes[i].detailsSubTypeId + "'>"
				+ subTypes[i].detailsSubTypeName + "</option>";
	}
	document.getElementById(selectId).innerHTML = "<div class= 'form-group'>"
			+ "<label for='detailsSubTypeId' class='col-sm-2 control-label'>Sub type:</label>"
			+ "<select id=" + subTypeSelect + ">"
			+ "<option value=''>Select a type:</option>" + typesContent
			+ "</select>" + "</div>" + "</div>";
}

function fillUpdateDetailsModal(travelDetailsId, travelId) {
	document.getElementById("updateDetailsModalLabel").innerHTML = "Edit details in: " + selectedTravelName; 
	for (var i = 0; i < allTravelDetails.length; i++) {
		if (allTravelDetails[i].travelDetailsId == travelDetailsId) {
			document.getElementById("updateDetailsType").value = allTravelDetails[i].detailsTypeId;
			showTypes("updateDetailsSubType", "updateDetailsSubTypeSelect",
					allTravelDetails[i].detailsTypeId);
			document.getElementById("updateDetailsSubTypeSelect").value = allTravelDetails[i].detailsSubTypeId;
			document.getElementById("updateDetailsDateFrom").value = allTravelDetails[i].detailsDateFrom;
			document.getElementById("updateDetailsDateTo").value = allTravelDetails[i].detailsDateTo;
			document.getElementById("updateDetailsBudget").value = allTravelDetails[i].detailsBudget;
			document.getElementById("updateDetailsComment").value = allTravelDetails[i].detailsComment;
			selectedTravelId = travelId;
			selectedTravelDetailsId = travelDetailsId;

		}
	}

}
// selle funktsiooni peaks hiljem universaalseks tegema
// (getNewDetailsSubTypeId())
function getUpdatedDetailsSubTypeId() {
	subTypeId = document.getElementById("updateDetailsSubTypeSelect").value;
	selectedDetailsSubTypeId = subTypeId;
}

// selle funktsiooni peaks hiljem universaalseks tegema
// addIdFieldInJson(objectJson, selectedTravelId,selectedDetailsSubTypeId)
function addUpdatedIdFieldInJson(objectJson, selectedTravelDetailsId,
		selectedTravelId, selectedDetailsSubTypeId) {
	var modifiedJson = objectJson;
	modifiedJson.travelDetailsId = selectedTravelDetailsId;
	modifiedJson.travelId = selectedTravelId;
	modifiedJson.detailsSubTypeId = selectedDetailsSubTypeId;
	return modifiedJson;
}

function updateDetails() {
	getUpdatedDetailsSubTypeId();
	var newDetailsJSON = $("#updateDetails").serializeFormJSON();
	newDetailsJSON = addUpdatedIdFieldInJson(newDetailsJSON,
			selectedTravelDetailsId, selectedTravelId, selectedDetailsSubTypeId);
	var updatedDetails = JSON.stringify(newDetailsJSON);
	$.ajax({
		url : "http://localhost:8080/TravelPlan/rest/traveldetails",
		cache : false,
		type : 'PUT',
		data : updatedDetails,
		contentType : "application/json; charset=utf-8",
		success : function() {
			viewDetailsTable(selectedContainerId, selectedTravelId,
					selectedTravelName);
			$('#updateDetailsModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

var deleteDetailslId;
function fillDeleteDetailsModal(detailsId) {
	document.getElementById("deleteDetailsModalBody").innerHTML = "Are you sure, you want to delete these details?";
	deleteDetailsId = detailsId;
}

function deleteDetails() {
	$.ajax({
		url : "http://localhost:8080/TravelPlan/rest/traveldetails/"
				+ deleteDetailsId,
		cache : false,
		type : 'DELETE',
		contentType : "application/json; charset=utf-8",
		success : function() {
			viewDetailsTable(selectedContainerId, selectedTravelId,
					selectedTravelName);
			$('#deleteDetailsModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

(function($) {
	$.fn.serializeFormJSON = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);
